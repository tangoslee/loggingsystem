# Unified Logging System

## Structure

```bash
                                 Unified Logging System Structure


        /log/{svc}/{plaform}
        e.g) /log/tendopay/wp
                              +----------------------------------------+
Plugins                       |                                        |
+---------+                +--+------+                       +---------+-+
|         |  +-----------> | 1.Nginx |                       |  5.Kibana | +----->  6.Admin
|         |                |         |                       |           |
+---------+  +-----------> +--+------+                       +---------+-+
             |                |                                        |
             |                |   +                           ^        |
             |                |   |                           |        |
+---------+  |                |   |                           +        |
|         | ++                |   v                                    |
|         |                   | 2.tendopay_error.log  +-----------+    |
+---------+                   |                       |           |    |
Tendopay Server               |       +               |           |    |
                              |       |               |           |    |
 e.g) /log/tendopay/api       |       v               |           |    |
                              |                       |           |    |
                              |    +--------+         |           |    |
                              |    |        | +---->  |           |    |
                              |    +--------+         +-----------+    |
                              |     3.Fluentd       4.Elasticsearch    |
                              |                                        |
                              +----------------------------------------+


```

## Trace Message Format

```json
{
  "message": [string, string, string...],
  "trace": [string, string, string...],
}
```

## Operations

### Download

```bash

git clone git@bitbucket.org:cddevteam/loggingsystem.git

```

### Start All (Nginx + Elasticsearch + Fluentd + Kibana)

```bash

docker-compose up -d

```

### Stop All (Nginx + Elasticsearch + Fluentd + Kibana)

```bash

docker-compose down

```


